﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libreriaRepaso
{
    public class Class1
    {
        public static int maximo(int numero1, int numero2)
        {
            return Math.Max(numero1, numero2);
        }
        public static int minimo(int numero1, int numero2)
        {
            return Math.Min(numero1, numero2);
        }
    }
}
